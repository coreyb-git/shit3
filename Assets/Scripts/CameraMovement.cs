﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

	public GameObject car;
	private Vector3 cameraFollow;

	// Use this for initialization
	void Start () {
		cameraFollow = transform.position - car.transform.position;
	}
	
	// Update is called once per frame
	void LateUpdate () {
		transform.position = car.transform.position + cameraFollow;
	}
}
