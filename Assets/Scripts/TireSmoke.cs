﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TireSmoke : MonoBehaviour {
	public GameObject tireSmoke;
	public GameObject tireSmoke2;
	public GameObject car;
	public Renderer tireSmokeRenderer;
	public Renderer tireSmokeRenderer2;

	public ParticleSystem smokeParticleSystem;
	public ParticleSystem smokeParticleSystem2;

	CarMovement CarMovementScript;

	// Use this for initialization
	void Start () {
		CarMovementScript = car.GetComponent<CarMovement> ();

		tireSmokeRenderer.enabled = false; 	// Disables smoke rendering at the start
		tireSmokeRenderer2.enabled = false; 	// Disables smoke rendering at the start
		smokeParticleSystem = tireSmokeRenderer.GetComponent<ParticleSystem> ();
		smokeParticleSystem2 = tireSmokeRenderer2.GetComponent<ParticleSystem> ();
		smokeParticleSystem.Play(withChildren:true);	//Starts smoke particles
		smokeParticleSystem2.Play(withChildren:true);	//Starts smoke particles

	}
	
	// Update is called once per frame
	void Update () {
		var mainSmokeParticleSystem = smokeParticleSystem.main; 	//Allows to change main smoke particle settings from script
		var mainSmokeParticleSystem2 = smokeParticleSystem2.main; 	//Allows to change main smoke particle settings from script
		mainSmokeParticleSystem.startDelay = 6.1f;		//Sets smoke delay so it's even with start
		mainSmokeParticleSystem2.startDelay = 6.1f;		//Sets smoke delay so it's even with start
		if((CarMovementScript.wheelVelocity> 0) && (CarMovementScript.currentTraction <1f)) {	// If there is no traction, there is smoke
			tireSmokeRenderer.enabled = true;
		}

		if((CarMovementScript.wheelVelocity2 > 0) && (CarMovementScript.currentTraction2 <1f)) {	// If there is no traction, there is smoke
			tireSmokeRenderer2.enabled = true;
		}

		if (CarMovementScript.currentTraction >= 1f)	tireSmokeRenderer.enabled = false;		// When there is 100% traction, there is no smoke
		if (CarMovementScript.currentTraction2 >= 1f)	tireSmokeRenderer2.enabled = false;		// When there is 100% traction, there is no smoke
	}
}
