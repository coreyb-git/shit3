﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GearBox : MonoBehaviour {

	Engine EngineScript;
	Timers TimersScript;
	CarMovement CarMovementScript;

	public GameObject engine;
	public GameObject timer;
	public GameObject car;

	private float finalDrive;	// Rack and Pinion ratio in gearbox
	private float finalDrive2;	// Rack and Pinion ratio in gearbox
	private float[] gearRatioArray = { 3.45f, 1.94f, 1.29f, 0.91f, 0.75f };		// Gear ratios 1-5
	private float[] gearRatioArray2 = { 3.45f, 1.94f, 1.29f, 0.91f, 0.75f };		// Gear ratios 1-5
	public float currentGearRatio; 	// Current gear ratio, pulled from gear ratio array
	public float currentGearRatio2; 	// Current gear ratio, pulled from gear ratio array
	public float gearEndRatio;		// Ratio when Final drive ratio and gear ratio is combined
	public float gearEndRatio2;		// Ratio when Final drive ratio and gear ratio is combined
	public int currentGear;		// Current gear
	public int currentGear2;		// Current gear
	private float gearShiftTime;	// How much time does gear changing takes
	private float gearShiftTime2;	// How much time does gear changing takes

	// Use this for initialization
	void Start () {

		EngineScript = engine.GetComponent<Engine> ();
		TimersScript = timer.GetComponent<Timers> ();
		CarMovementScript = car.GetComponent<CarMovement> ();

		finalDrive = 3.94f;
		finalDrive2 = 3.94f;
		currentGear = 1;
		currentGear2 = 1;
		currentGearRatio = gearRatioArray [currentGear];
		currentGearRatio2 = gearRatioArray2 [currentGear2];
		gearShiftTime = 6.5f;
		gearShiftTime2 = 6.5f;
		gearEndRatio = finalDrive / currentGearRatio;
		gearEndRatio2 = finalDrive2 / currentGearRatio2;

	}
	
	// Update is called once per frame
	void Update () {

		currentGearRatio = gearRatioArray[currentGear];
		currentGearRatio2 = gearRatioArray2[currentGear2];


		// Gear shifting
		if (EngineScript.engineRpm >= EngineScript.maxRpm) {	// Time to switch to the next gear
			currentGear++;	// Switches to the next gear
			currentGearRatio = gearRatioArray [currentGear];
			gearEndRatio = finalDrive / currentGearRatio;
			EngineScript.engineRpm = (CarMovementScript.wheelVelocity * CarMovementScript.velocityNormalizer) / gearEndRatio;		// resets engine's rpm for next gear
			TimersScript.startCountdown = gearShiftTime;	// Adds time for shifting gear
		}

		if (EngineScript.engineRpm2 >= EngineScript.maxRpm2) {	// Time to switch to the next gear
			currentGear2++;	// Switches to the next gear
			currentGearRatio2 = gearRatioArray2 [currentGear2];
			gearEndRatio2 = finalDrive2 / currentGearRatio2;
			EngineScript.engineRpm2 = (CarMovementScript.wheelVelocity2 * CarMovementScript.velocityNormalizer) / gearEndRatio2;		// resets engine's rpm for next gear
			TimersScript.startCountdown2 = gearShiftTime2;	// Adds time for shifting gear
		}

	}
}
